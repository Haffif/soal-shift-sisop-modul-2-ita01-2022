#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>

void process(char bash[], char *arg[]){
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execv(bash, arg);
    }
    else{
        ((wait(&status))>0);
    }
}

void process_mv(char bash[], char file[], char target[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, target, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}

void process_rm(char bash[], char file[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}

int main() {
    char *user;
    user=(char *)malloc(10*sizeof(char));
    user=getlogin();
    char anim[] = "/home/ubuntu/modul2/animal";
    char *alam[] = {"/home/ubuntu/modul2/darat", "/home/ubuntu/modul2/air"};
    
    char *makefoldermodul[] = {"mkdir", "/home/ubuntu/modul2", NULL};
    char *makefolderdarat[] = {"mkdir", "/home/ubuntu/modul2/darat", NULL};
    char *makefolderair[] = {"mkdir", "/home/ubuntu/modul2/air", NULL};
    char *unzipanim[] =  {"unzip", "animal.zip", "-d", "/home/ubuntu/modul2", NULL};

    //bikin folder-folder
    process("/bin/mkdir", makefoldermodul);
    process("/bin/mkdir", makefolderdarat);
    sleep(3); //jeda 3 detik
    process("/bin/mkdir", makefolderair);

    //unzip animal
    process("/usr/bin/unzip", unzipanim);

    //search n rescue
    DIR *dp;
    struct dirent *ep;

    dp = opendir(anim);
    if(dp != NULL) {
        while((ep = readdir(dp))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "darat")) {
                char path[100] = "/home/ubuntu/modul2/animal/";
                strcat(path, ep->d_name); 
                process_mv("mv", path, "/home/ubuntu/modul2/darat");
            } else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "air")) {
                char path[100] = "/home/ubuntu/modul2/animal/";
                strcat(path, ep->d_name); 
                process_mv("mv", path, "/home/ubuntu/modul2/air");
            } else { // exception dir "." and ".."
                if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
                    char path[100] = "/home/ubuntu/modul2/animal/";
                    strcat(path, ep->d_name); 
                    process_rm("rm", path);
                }
                
            }
        }
        (void) closedir (dp);
    } else  perror ("Couldn't open the directory");

    // kill birds
    DIR *darat;

    darat = opendir(alam[0]);
    if(darat != NULL) {
        while((ep = readdir(darat))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "bird")) {
                char path[100] = "/home/ubuntu/modul2/darat/";
                strcat(path, ep->d_name); 
                process_rm("rm", path);
            }
        }
        (void) closedir (darat);
    } else  perror ("Couldn't open the directory");

    // make list
    DIR *air;

    air = opendir(alam[1]);
    FILE *air_list = fopen("/home/ubuntu/modul2/air/list.txt", "w");

    struct stat fs;
    int r;

    if(air != NULL) {
         while((ep = readdir(air))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "jpg")) {
                char path[100] = "/home/ubuntu/modul2/air/";
                strcat(path, ep->d_name);
                r = stat(path, &fs);
                char pread = '_', pwrite = '_', pexec= '_';
                if( fs.st_mode & S_IRUSR ) pread = 'r';
                if( fs.st_mode & S_IWUSR ) pwrite = 'w';
                if( fs.st_mode & S_IXUSR ) pexec = 'x';
                fprintf(air_list, "%s_%c%c%c_%s\n", user, pread, pwrite, pexec, ep->d_name);
            }
        }
        (void) closedir (air);
    } else  perror ("Couldn't open the directory");

    fclose(air_list);

}
