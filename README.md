# soal-shift-sisop-modul-2-ITA01-2022

---

Hasil Pengerjaan Soal Shift Praktikum SISTEM OPERASI 2022
Anggota Kelompok:
1. Haffif Rasya Fauzi (5027201002)
2. Muhammad Faris Anwari (5027201008)
3. Muhammad Dzakwan (5027201065)

---

## Soal 1
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

<ol type="a">
    <li>Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).</li>
    <li>Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA</li>
    <li>Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.</li>
    <li>Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.<br><b>Contoh : 157_characters_5_Albedo_53880</b></li>
    <li>Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)</li>
</ol>

**Note:**
- Menggunakan fork dan exec
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh 1 script di latar belakang. Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.
- Link:

```
Database item characters :
https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
Database item weapons :
https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
```

**Tips:**
- Gacha adalah proses untuk mendapatkan suatu item dengan cara melakukan randomize dari seluruh item yang ada.
- Dikarenakan file database memiliki format (.json). Silahkan gunakan library <json-c/json.h>, install dengan “apt install libjson-c-dev”, dan compile dengan “gcc [nama_file] -l json-c -o [nama_file_output]”. Silahkan gunakan dokumentasi berikut untuk membaca dan parsing file (.json).

```
https://progur.com/2018/12/how-to-parse-json-in-c.html
```


### Penyelesaian

Kode:<br>
[Source Code](./soal1)

---

Praktikan diminta mengunduh database item characters dan weapons, lalu mengekstrak kedua file tersebut. Kemudian praktikan membuat folder `gacha_gacha` untuk menampung hasil gacha.

```c#
char *dbase[] = {
        "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};
char *file[] = {"chara.zip", "weapon.zip"};
char *dirgacha[] = {"gacha_gacha"};

dbdownload(dbase[0], file[0]);
dbdownload(dbase[1], file[1]);
unzip(file[0]);
unzip(file[1]);
```

```c#
void dbdownload(char *dbase, char *file)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "-q", dbase, "-O", file, NULL};
        execv("/bin/wget", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```

```c#
void unzip(char *file)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"unzip", "-q", "-n", file, NULL};
        execv("/bin/unzip", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```

- Praktikan mendefinisikan terlebih dahulu tautan-tautan sumber unduhan database, serta nama file untuk menampungnya
- Fungsi `dbdownload` menerima argumen tautan database serta nama file yang akan jadi output, kemudian mengunduh database dengan metode `wget`. Adapun flag `--no-check-certificate` diperlukan agar dapat mengunduh dari Google Drive, serta flag `-q` agar proses berjalan di background.
- Fungsi `unzip` menerima argumen `file` yang akan diunzip

```c#
mkprocess(dirgacha[0]);
```

```c#
void mkprocess(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        DIR *checkDir = opendir(dir);
        if (checkDir)
        {
            closedir(checkDir); 
        }
        else
        {
            char *argv[] = {"mkdir", dir, NULL}; 
            execv("/bin/mkdir", argv);
        }
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
- Fungsi `mkprocess` guna membuat direktori yang diterima lewat argumen `dir`

Fungsi-fungsi berikut menjadi bagian berjalannya sistem gacha:

```c#
int rannum(int len)
{
    return (rand() % len);
}
```
- Meng-generate bilangan acak dengan batas maksimal sesuai yang ditetapkan `len` dimana bilangan akan di-mod oleh `len` sehingga tidak mungkin melampaui

```c#
void reduceprim(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 100)
    {
        zip();
        rmprocess("weapons");
        rmprocess("characters");
        stop();
    }
}
```
- Mengurangi jumlah primogem sebesar **160** tiap dilakukan proses gacha, dimana jumlah primogem ditentukan sebelumnya sebesar **79000**. Adapun jika primogem telah di bawah jumlah yang diperlukan untuk gacha, maka memanggil fungsi `zip` dan `rmprocess`

```c#
void zip()
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        chdir("/home/rasy/sisop/soal-shift-sisop-modul-2-ita03-2022/soal1/gacha_gacha/");
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", ".", "-i", "*", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
- Mengzip direktori `gacha_gacha` dengan keluaran sesuai ketentuan soal, yakni nama output `not_safe_for_wibu.zip` dengan password `satuduatiga`

```c#
void rmprocess(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"rm", "-rf", dir, NULL};
        execv("/bin/rm", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
- Menghapus direktori yang ditentukan `dir`

Praktikan diminta membuat sistem gacha, dimana pada tiap kali jumlah gacha bernilai genap maka dilakukan gacha weapon, dan bila ganjil maka gacha character. Pada tiap jumlah gacha kelipatan 10 atau *tenpull*, output dimuat dalam file text baru. Pada tiap jumlah gacha kelipatan 90 atau *hard pity*, dibuat folder baru yang menampung hasil pencatatan 90 kali gacha sebelumnya.

```c#
void gachaproc(int nth, char gen_file[100])
{
    int len;
    char basepath[100], type[100];

    if (nth % 2 != 0)
    {
        len = 48;
        strcpy(basepath, "characters/");
        strcpy(type, "Characters");
    }
    if (nth % 2 == 0)
    {
        len = 130;
        strcpy(basepath, "weapons/");
        strcpy(type, "Weapons");
    }

    struct dirent *dp;
    DIR *dir = opendir(basepath);

    char temppath[50];
    int rn_gacha = rannum(len);
    if (dir != NULL)
    {
        for (int k = 0; k <= rn_gacha; k++)
        {
            dp = readdir(dir);
        }
        strcpy(temppath, dp->d_name);
        dp = NULL;
        closedir(dir);
    }
    else
    {
        perror("Couldn't open the directory");
    }

    strcat(basepath, temppath);
    FILE *fp;
    char temp[4096];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    fp = fopen(basepath, "r");
    fread(temp, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(temp);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    json_object_object_get_ex(parsed_json, "name", &name);

    FILE *output;
    output = fopen(gen_file, "a");

    char n[5], rest_prim[10];
    sprintf(rest_prim, "%d", primonum);
    sprintf(n, "%d", nth);

    char fileContext[100];

    strcpy(fileContext, n);
    strcat(fileContext, "_");
    strcat(fileContext, type);
    strcat(fileContext, "_");
    strcat(fileContext, json_object_get_string(rarity));
    strcat(fileContext, "_");
    strcat(fileContext, json_object_get_string(name));
    strcat(fileContext, "_");
    strcat(fileContext, rest_prim);
    strcat(fileContext, "\n");
    fputs(fileContext, output);
    fclose(output);
}
```

```c#
if (nth % 2 != 0)
{
  len = 48;
  strcpy(basepath, "characters/");
  strcpy(type, "Characters");
}
if (nth % 2 == 0)
{
  len = 130;
  strcpy(basepath, "weapons/");
  strcpy(type, "Weapons");
}

struct dirent *dp;
DIR *dir = opendir(basepath);
```
- Bila gacha berada pada kali yang ganjil, sistem akan menuju database character dan menetapkan `len` sebesar **48** yakni jumlah karakter. Jika genap, maka menuju database weapon dan `len` sebesar **130**

```c#
char temppath[50];
int rn_gacha = rannum(len);
if (dir != NULL)
{
    for (int k = 0; k <= rn_gacha; k++)
    {
        dp = readdir(dir);
    }
    strcpy(temppath, dp->d_name);
    dp = NULL;
    closedir(dir);
}
else
{
    perror("Couldn't open the directory");
}
```
- Melakukan directory listing pada database yang ditentukan, baik character maupun weapon. `rn_gacha` sebagai bilangan acak yang digenerate menentukan item yang diperoleh. Nama dari item akan dicopy pada `temppath`

```c#
strcat(basepath, temppath);
  FILE *fp;
  char temp[4096];
  struct json_object *parsed_json;
  struct json_object *name;
  struct json_object *rarity;

  fp = fopen(basepath, "r");
  fread(temp, 4096, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(temp);
  json_object_object_get_ex(parsed_json, "rarity", &rarity);
  json_object_object_get_ex(parsed_json, "name", &name);

  FILE *output;
  output = fopen(gen_file, "a");

  char n[5], rest_prim[10];
  sprintf(rest_prim, "%d", primonum);
  sprintf(n, "%d", nth);
```
- Membaca dan memparsing item menggunakan library `json-c/json.h`, hingga didapat nama dan rarity item
- Mendapat sisa primogem serta kali gacha keberapa

```c#
char fileContext[100];

strcpy(fileContext, n);
strcat(fileContext, "_");
strcat(fileContext, type);
strcat(fileContext, "_");
strcat(fileContext, json_object_get_string(rarity));
strcat(fileContext, "_");
strcat(fileContext, json_object_get_string(name));
strcat(fileContext, "_");
strcat(fileContext, rest_prim);
strcat(fileContext, "\n");
fputs(fileContext, output);
fclose(output);
```
- Menyusun file sesuai ketentuan soal

---

Proses `gachaproc` ter-contain dalam `int main()`, dimana program mengunduh database dan mengunzipnya. Program juga menginisiasi hitungan gacha pada `gachacount`.

```c#
int gachacount = 0;

dbdownload(dbase[0], file[0]);
dbdownload(dbase[1], file[1]);
unzip(file[0]);
unzip(file[1]);

mkprocess(dirgacha[0]);
```
Program membuat folder untuk menyimpan tiap kelipatan gacha 90

```c#
int cekpity90 = gachacount - (gachacount % 90);
char path[100], hitpity90[5];

strcpy(path, dirgacha[0]);
strcat(path, "/total_gacha_");
sprintf(hitpity90, "%d", cekpity90);
strcat(path, hitpity90);
strcat(path, "/");

mkprocess(path);
```

Program mengiterasi sebanyak 9 kali untuk menggacha sebanyak 90 kali

```c#
for (int i = 0; i < 9; i++)
{
  char gen_file[100], round[5];
  struct tm *ptr;
  time_t rawTime;
  rawTime = time(NULL);
  ptr = localtime(&rawTime);
  char temp[100];
  strftime(temp, 100, "%H:%M:%S", ptr);

  int checkRound = gachacount - (gachacount % 10);

  strcpy(gen_file, path);
  strcat(gen_file, "/");
  strcat(gen_file, temp);
  strcat(gen_file, "_gacha_");
  sprintf(round, "%d", checkRound);
  strcat(gen_file, round);
  strcat(gen_file, ".txt");

  for (size_t j = 0; j < 10; j++)
  {
    gachacount++;
    reduceprim(&primonum);
    gachaproc(gachacount, gen_file);
  }
  sleep(1);

  srand(time(NULL));
}
```

- Program membaca timestamp untuk memformat nama file gacha
- Tiap iterasi dimana gacha dilakukan, `gachacount` bertambah, serta fungsi `reduceprim` dipanggil agar jumlah total primogem berkurang
- Program melakukan `sleep(1)` agar tiap file dibuat per satu detik

Untuk hasil output dari program yang telah dijalankan adalah sebagai berikut:
![output 1_1](./img/output 1_1.png)

Untuk isi dari file `not_safe_for_wibu.zip` adalah sebagai berikut:
![output 1_2](./img/output 1_2.png)


![output 1_3](./img/output 1_3.png)

Untuk contoh salah satu file hasil gatcha adalah sebagai berikut:
![output 1_4](./img/output 1_4.png)

### Kendala
Kendala yang dihadapi kelompok kami dalam mengerjakan soal nomor 1 yaitu kesulitan dalam melakukan download dari gdrive. Akan tetapi, hal tersebut sudah dapat terselesaikan.

## Soal 2

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

<ol type="a">
    <li>Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.</li>
	<li>Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.<br>Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.</li>
    <li>Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.<br>Contoh: “/drakor/romance/start-up.png”.</li>
    <li>Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.</li>
    <li>Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). <b>Format harus sesuai contoh dibawah ini.</b></li>
</ol>

```
kategori : romance

nama : start-up
rilis  : tahun 2020

nama : twenty-one-twenty-five
rilis  : tahun 2022
```

**Note dan ketentuan Soal:**
- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.


### Penyelesaian

Kode:<br>
[Source Code](./soal2)


#### 2.a Mengekstrak zip ke folder dan menghapus folder yang tidak penting di zip

Pertama, kelompok kami mendeklarasikan library-library yang diperlukan untuk menjalankan program yang kami buat sebagai berikut:

```c#
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
```

Selanjutnya, kami membuat beberapa variable global untuk melakukan perintah `execv()`. Untuk kodenya yaitu sebagai berikut:

```c#
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```

Lalu, pada fungsi main akan dipanggil beberapa fungsi untuk menjalankan program-program yang diminta. Untuk kodenya yaitu sebagai berikut:

```c#
int main()
{
  createParentFolder();
  extractZip();
  removeDir();
  createFolderDrakorByCategory();
  moveFileByCategory();
  getListFolderDrakor();
}
```

Kemudian, untuk menjalankan perintah `execv()` secara berulang maka membuat sebuah fungsi yang bertugas untuk melakukan atau membuat suatu proses. Lalu, fungsi ini akan dipanggil secara terus menerus pada fungsi `main()`. Untuk kode dari fungsi proses yaitu:

```c#
void createProcess(char *str, char *argv[])void moveFileByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/rasy/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/rasy/shift2/drakor/";
          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define destination
          strcat(destination, token3);
          strcat(destination, "/");

          // define file sources
          strcat(fileSources, fileName);

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath[100] = "/home/rasy/shift2/drakor/";
          char destination1[100];
          char destination2[100];
          char fileSources1[100];
          char fileSources2[100];

          // copy the corePath
          strcpy(destination1, corePath);
          strcpy(destination2, corePath);
          strcpy(fileSources1, corePath);
          strcpy(fileSources2, corePath);

          // define file sources
          strcat(fileSources1, fileName);
          strcat(fileSources2, fileName);

          // define destination1
          strcat(destination1, token53);
          strcat(destination1, "/");

          // define destination2
          strcat(destination2, token55);
          strcat(destination2, "/");

          char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
          createProcess(keyCopy, copyFile53);
          char *moveFile55[] = {"mv", fileSources2, destination2, NULL};
          createProcess(keyMove, moveFile55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}
```

Setelah itu, kami membuat folder drakor yang terdapat dalam folder shift2 pada directory home. Adapun fungsi untuk melakukan hal tersebut yaitu `createParentFolder()`. Lalu, melakukan ekstrak`drakor.zip` yang akan disimpan pada path yang telah ditentukan sebelumnya yaitu `/home/rasy/modul2` yang akan dilakukan oleh fungsi `extractZip()`. Kemudian, untuk menghapus folder hasil extract zip yang tidak diinginkan (tidak dalam format png) akan dilakukan oleh sebuah fungsi yang bernama `removeDir()`. Untuk kode dari ketiga fungsi yang dijelaskan sebelumnya adalah sebagai berikut:

```c#
void createParentFolder()
{
  char *argv[] = {"mkdir", "-p", "/home/rasy/shift2/drakor/", NULL};
  createProcess(keyMkdir, argv);
}

void extractZip()
{
  char *argv[] = {"unzip", "-qo", "/home/rasy/modul2/drakor.zip", "-d", "/home/rasy/shift2/drakor", NULL};
  createProcess(keyUnzip, argv);
}

void removeDir()
{
  char *argv[] = {"rm", "-r", "/home/rasy/shift2/drakor/coding/", "/home/rasy/shift2/drakor/__MACOSX/", "/home/rasy/shift2/drakor/song/", "/home/rasy/shift2/drakor/trash", NULL};
  createProcess(keyRemove, argv);
}
```

Untuk hasil output dari program yang telah dijalankan adalah sebagai berikut:
![output 2a](./img/output 2a.png)

#### 2.b Mengkategorikan poster sesuai dengan jenis kategorinya

Untuk mengkategorikan poster sesuai dengan jenis kategorinya, langkah pertama yang dilakujan adalah membuat sebuah fungsi `createFolderDrakorByCategory()`. Pada fungsi tersebut dibuat sebuah algoritma untuk melihat semua isi folder hasil ekstrak zip. Selanjutnya, menjalankan perintah `execv()` yang akan mengeksekusi command `mkdir` untuk setiap folder yang masih belum terdapat kategorinya. Kemudian, menggunakan `strtok()` yang berfungsi untuk memisahkan setiap potongan nama file. untuk kode dari fungsi `createFolderDrakorByCategory()` yaitu sebagai berikut:

```c#
void createFolderDrakorByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/rasy/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/rasy/shift2/drakor/";
          strcat(corePath, token3);
          char *createFolder3[] = {"mkdir", "-p", corePath, NULL};
          createProcess("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath1[100] = "/home/rasy/shift2/drakor/";
          char corePath2[100] = "/home/rasy/shift2/drakor/";
          strcat(corePath1, token53);
          strcat(corePath2, token55);
          char *createFolder53[] = {"mkdir", "-p", corePath1, NULL};
          char *createFolder55[] = {"mkdir", "-p", corePath2, NULL};
          createProcess(keyMkdir, createFolder53);
          createProcess(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Untuk hasil output dari program yang telah dijalankan adalah sebagai berikut:
![output 2b](./img/output 2b.png)

#### 2.c Melakukan pemindahan poster sesuai kategori pada folder

Untuk melakukan pemindahan poster sesuai kategori pada folder maka dibuat sebuah fungsi dengan nama `moveFileByCategory()`. Pada fungsi tersebut digunakan untuk memindahkan isi dari folder `/home/rasy/shift2/drakor/` yang berisi folder-folder per-kategori yang telah dibuat dan file poster drakor. Kemudian, melakukan list terhadap semua file ke dalam folder yang sesuai. Dimana dalam menyeleksi file pada setiap folder sesuai kategorinya menggunakan `strtok()` dengan parameter pemisahnya yaitu `;`. Ketentuan yang ditentukan apabila file yang hasil katanya terdapat tiga maka folder tersebut akan menuju sesuai kata ketiganya (menggunakan move). Lalu, apabila file yang hasil katanya terdapat lima maka akan dipindahkan kedua folder. Untuk tujuan dari folder pertama sesuai dengan kata ketiganya (menggunakan copy), dan arah folder keduanya sesuai dengan kata ke-5nya (menggunakan move). Disini kelompok kami hanya baru memindahkan filenya (belum melakukan rename terhadap file). Untuk kode dari fungsi `moveFileByCategory()` yaitu sebagai berikut:

```c#
void moveFileByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/rasy/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/rasy/shift2/drakor/";
          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define destination
          strcat(destination, token3);
          strcat(destination, "/");

          // define file sources
          strcat(fileSources, fileName);

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath[100] = "/home/rasy/shift2/drakor/";
          char destination1[100];
          char destination2[100];
          char fileSources1[100];
          char fileSources2[100];

          // copy the corePath
          strcpy(destination1, corePath);
          strcpy(destination2, corePath);
          strcpy(fileSources1, corePath);
          strcpy(fileSources2, corePath);

          // define file sources
          strcat(fileSources1, fileName);
          strcat(fileSources2, fileName);

          // define destination1
          strcat(destination1, token53);
          strcat(destination1, "/");

          // define destination2
          strcat(destination2, token55);
          strcat(destination2, "/");

          char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
          createProcess(keyCopy, copyFile53);
          char *moveFile55[] = {"mv", fileSources2, destination2, NULL};
          createProcess(keyMove, moveFile55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Untuk hasil output dari program yang telah dijalankan adalah sebagai berikut:
![output 2c](./img/output 2c.png)

#### 2.d dan 2e Memindahkan gambar yang terdapat dua poster sesuai kategorinya dan membuat data.txt

Untuk memindahkan gambar yang terdapat dua poster sesuai kategorinya dan membuat data.txt digabungkan karena terdapat korelasi antar satu sama lainnya. Setelah memindahkan setiap file, maka akan dilakukan rename pada setiap kategorinya dan membuat `data.txt`. Kemudian untuk melakukan hal tersebut maka akan dibuat sebuah fungsi dengan nama `getListFolderDrakor()` yang berfungsi untuk melakukan list dari semua isi folder `/home/rasy/shift2/drakor/`. Setiap iterasi dalam folder tersebut akan dipanggil sebuah fungsi dengan nama `renameFileAndCreateTxtPerFolder()` dengan mengikutkan nama folder dari hasil iterasi sekarang pada argumennya. Pada function `renameFileAndCreateTxtPerFolder()` kami akan melakukan list file yang ada untuk folder yang spesifik. Untuk setiap filenya, kami akan menjalanakan fungsi `strtok()` dengan pemisah tanda `;`. 

Pada file yang hasil katanya terdapat tiga, maka akan dilakukan rename sesuai dengan kata pertamanya (nama drakor). Lalu data nama dan tahun (kata kedua) akan disimpan pada sebuah array di dalam fungsi `renameFileAndCreateTxtPerFolder()`. Kemudian pada file yang hasil katanya terdapat lima, maka akan ditentukan terlebih dahulu apakah kata ketiga atau kelimanya sesuai dengan nama folder dimana file tersebut berada sekarang lalu kemudian akan dilakukan rename. Selain itu juga akan menyimpan nama file dan nama tahunnya kedalam sebuah array. Setelah perulangan dalam function `renameFileAndCreateTxtPerFolder()` selesai, lalu akan memanggil fungsi `insertionSort()` yang akan melakukan sorting secara ascending untuk array namaFile dan tahun yang dibuat sebelumnya. Terakhir melakukan print pada file `data.txt` sesuai dengan format yang ditentukan.

```c#
void insertionSort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
  int i, key, j;
  char kuy[100];
  for (i = 1; i < n; i++)
  {
    key = arrTahun[i][0];
    strcpy(kuy, arrFilename[i]);
    j = i - 1;

    while (j >= 0 && arrTahun[j][0] > key)
    {
      arrTahun[j + 1][0] = arrTahun[j][0];
      strcpy(arrFilename[j + 1], arrFilename[j]);
      j = j - 1;
    }
    arrTahun[j + 1][0] = key;
    strcpy(arrFilename[j + 1], kuy);
  }
}

void renameFileAndCreateTxtPerFolder(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "/home/rasy/shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char corePath[100] = "/home/rasy/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define file sources
          strcat(fileSources, fileName);

          // define destination
          strcat(destination, arr[0]);
          strcat(destination, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char corePath[100] = "/home/rasy/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define file sources
          strcat(fileSources, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            // define destination
            strcat(destination, arr[0]);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            // define destination
            strcat(destination, token522);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile);
        }
      }
    }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    createProcess(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    insertionSort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void getListFolderDrakor()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/rasy/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        renameFileAndCreateTxtPerFolder(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Untuk hasil output dari program yang telah dijalankan adalah sebagai berikut:
![output 2e_1](./img/output 2e_1.png)

Kemudian, untuk salah satu contoh data.txt adalah sebagai berikut:
![output 2e_2](./img/output 2e_2.png)

### Kendala
Kendala yang kami hadapi dalam mengerjakan soal nomor 2 yaitu kesulitan dalam menentukan algoritma mendapatkan data.txt dari tiap poster.

## Soal 3

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang.
<ol type="a">
    <li>Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.</li> 
    <li>Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.</li>
    <li>Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.</li>
    <li>Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.</li>
    <li>Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.<br>Contoh : conan_rwx_hewan.png</li>

</ol>

**Catatan:**
- Tidak boleh memakai system()
- Tidak boleh memakai function C mkdir() ataupun rename()
- Gunakan exec dan fork
- Direktori "." dan ".." tidak termasuk

### Penyelesaian

Kode:<br>
[Source Code](./soal3)

#### 3.a Membuat folder

Praktikan diminta membuat direktori `/home/[USER]/modul2/darat` dan `/home/[USER]/modul2/air` dengan jeda 3 detik antara pembuatan kedua direktori.

```c#
void process(char bash[], char *arg[]){
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execv(bash, arg);
    }
    else{
        ((wait(&status))>0);
    }
}
```

```c#
char *makefoldermodul[] = {"mkdir", "/home/ubuntu/modul2", NULL};
char *makefolderdarat[] = {"mkdir", "/home/ubuntu/modul2/darat", NULL};
char *makefolderair[] = {"mkdir", "/home/ubuntu/modul2/air", NULL};

//bikin folder-folder
    process("/bin/mkdir", makefoldermodul);
    process("/bin/mkdir", makefolderdarat);
    sleep(3); //jeda 3 detik
    process("/bin/mkdir", makefolderair);
```
- Pada balok kode pertama, praktikan mendefinisikan fungsi fork dan exec, yang akan menerima jalur bash dan argumen pada `void process(char bash[], char *arg[])`
- Praktikan pun lanjut membuat direktori modul2, darat dan air sesuai ketentuan soal

#### 3.b Mengekstrak animal.zip

Praktikan diminta mengekstrak animal.zip di dalam `/home/ubuntu/modul2`

```c#
char *unzipanim[] =  {"unzip", "animal.zip", "-d", "/home/ubuntu/modul2", NULL};

//unzip animal
    process("/usr/bin/unzip", unzipanim);
```

Output 3.a dan 3.b<br>
![output 3a dan 3b](./img/output 3a 3b.PNG)

#### 3.c Menyaring hewan darat dan air

Praktikan diminta memasukkan file foto hewan darat ke `/home/ubuntu/modul2/darat` dan air ke `/home/ubuntu/modul2/air`, serta menghapus hewan yang tidak keduanya. Hal ini dilakukan dengan mencari file yang memiliki substring `darat` atau `air` pada nama filenya.

```c#
void process_mv(char bash[], char file[], char target[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, target, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}

void process_rm(char bash[], char file[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}
```

```c#
char anim[] = "/home/ubuntu/modul2/animal";

//search n rescue
    DIR *dp;
    struct dirent *ep;

    dp = opendir(anim);
    if(dp != NULL) {
        while((ep = readdir(dp))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "darat")) {
                char path[100] = "/home/ubuntu/modul2/animal/";
                strcat(path, ep->d_name); 
                process_mv("mv", path, "/home/ubuntu/modul2/darat");
            } else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "air")) {
                char path[100] = "/home/ubuntu/modul2/animal/";
                strcat(path, ep->d_name); 
                process_mv("mv", path, "/home/ubuntu/modul2/air");
            } else { // exception dir "." and ".."
                if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
                    char path[100] = "/home/ubuntu/modul2/animal/";
                    strcat(path, ep->d_name); 
                    process_rm("rm", path);
                }
                
            }
        }
        (void) closedir (dp);
    } else  perror ("Couldn't open the directory");
```
- Praktikan mendefinisikan fungsi yang akan memindahkan dan menghapus file, yakni `void process_mv(char bash[], char file[], char target[])` dan `void process_rm(char bash[], char file[])`
- Membuka dan mengiterasi isi direktori `/home/ubuntu/modul2/animal`; apabila nama file mengandung substring `darat`, maka akan dipindahkan ke direktori `darat`, begitupun pada file dengan substring `air`, ke direktori `air`
- Adapun sisanya dihapus

#### 3.d Menghapus burung

Praktikan diminta menghapus hewan yang masuk kategori burung di dalam direktori `darat`.

```c#
char *alam[] = {"/home/ubuntu/modul2/darat", "/home/ubuntu/modul2/air"};

// kill birds
    DIR *darat;

    darat = opendir(alam[0]);
    if(darat != NULL) {
        while((ep = readdir(darat))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "bird")) {
                char path[100] = "/home/ubuntu/modul2/darat/";
                strcat(path, ep->d_name); 
                process_rm("rm", path);
            }
        }
        (void) closedir (darat);
    } else  perror ("Couldn't open the directory");
```
- Membuka dan mengiterasi isi direktori `home/ubuntu/modul2/darat`; apabila nama file mengandung substring `bird` maka akan dihapus

#### 3.e Membuat list hewan air
Praktikan diminta membuat list nama semua hewan yang ada di directory `/home/[USER]/modul2/air` ke `list.txt` dengan format `UID_[UID file permission]_Nama File.[jpg/png]` dimana UID adalah user dari file tersebut dan file permission adalah permission dari file tersebut.

```c#
// make list
    DIR *air;

    air = opendir(alam[1]);
    FILE *air_list = fopen("/home/ubuntu/modul2/air/list.txt", "w");

    struct stat fs;
    int r;

    if(air != NULL) {
         while((ep = readdir(air))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "jpg")) {
                char path[100] = "/home/ubuntu/modul2/air/";
                strcat(path, ep->d_name);
                r = stat(path, &fs);
                char pread = '_', pwrite = '_', pexec= '_';
                if( fs.st_mode & S_IRUSR ) pread = 'r';
                if( fs.st_mode & S_IWUSR ) pwrite = 'w';
                if( fs.st_mode & S_IXUSR ) pexec = 'x';
                fprintf(air_list, "%s_%c%c%c_%s\n", user, pread, pwrite, pexec, ep->d_name);
            }
        }
        (void) closedir (air);
    } else  perror ("Couldn't open the directory");

    fclose(air_list);
```

Output 3.e<br>
![output 3e](./img/output 3e.PNG)

Isi direktori `darat`<br>
![isi darat](./img/isi darat.PNG)

Isi direktori `air`<br>
![isi air](./img/isi air.PNG)

### Kendala

Praktikan mengalami kendala dalam mengiterasi isi suatu direktori dan memanipulasinya, namun setelah mempelajari materi lebih lanjut, praktikan menggunakan metode `opendir` dan `readdir`.
